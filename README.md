# Raspberry Pi 2 using QEMU

Demo to dive in QEMU world using Raspberry Pi 2 hardware as base of testing.

(based on https://openclassrooms.com/fr/courses/5281406-creez-un-linux-embarque-pour-la-domotique/5464241-emulez-une-raspberry-pi-avec-qemu)

## Requirements

First check if you have all requirements by checking the ARM CPU available.

```bash
qemu-system-arm -machine help
qemu-system-arm -machine raspi2 -cpu help
```

# Using modified Raspberry 2 kernel

More limited than native, but works.

## Install and steps

```bash
#pull https://github.com/dhruvvyas90/qemu-rpi-kernel
git submodule update --init --recursive
```

```bash
mkdir -p raspbian
cd raspbian
wget http://downloads.raspberrypi.org/raspbian_lite/images/raspbian_lite-2017-12-01/2017-11-29-raspbian-stretch-lite.zip
unzip 2017-11-29-raspbian-stretch-lite.zip
cd -
```

## Start

```bash
qemu-system-arm -M versatilepb \
                -cpu arm1176 \
                -kernel $PWD/dhruvvyas90/qemu-rpi-kernel/kernel-qemu-4.14.79-stretch \
                -append "root=/dev/sda2 panic=1 rootfstype=ext4 rw" \
                -hda $PWD/raspbian/2017-11-29-raspbian-stretch-lite.img \
                -m 256 \
                -dtb $PWD/dhruvvyas90/qemu-rpi-kernel/versatile-pb.dtb \
                -no-reboot \
                -net nic -net user,hostfwd=tcp::5022-:22 
```


# Using native Raspberry 2 kernel: unstable

## Install and steps

```bash
mkdir -p raspbian
cd raspbian
wget http://downloads.raspberrypi.org/raspbian_lite/images/raspbian_lite-2018-04-19/2018-04-18-raspbian-stretch-lite.zip
unzip 2018-04-18-raspbian-stretch-lite.zip
cd -
```

```bash
sudo mkdir /mnt/rpi
sudo losetup -f --show -P ./raspbian/2018-04-18-raspbian-stretch-lite.img
sudo mount /dev/loop<AUTO GENERATED ID>p1 /mnt/rpi
cp /mnt/rpi/kernel7.img /mnt/rpi/bcm2709-rpi-2-b.dtb ./raspbian
sudo umount /mnt/rpi
sudo losetup -d /dev/loop0
```

## Start

```bash
qemu-system-arm -M raspi2 \
        -cpu cortex-a7 \
        -append "rw earlyprintk loglevel=8 dwc_otg.lpm_enable=0 root=/dev/mmcblk0p2" \
        -dtb $PWD/raspbian/bcm2709-rpi-2-b.dtb \
        -drive file=$PWD/raspbian/2018-04-18-raspbian-stretch-lite.img,if=sd,format=raw \
        -kernel $PWD/raspbian/kernel7.img \
        -m 1G \
        -smp 4
```